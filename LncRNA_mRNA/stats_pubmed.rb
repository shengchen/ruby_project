#!/usr/bin/env ruby
# coding: utf-8

require 'bio'

usage = "#{$0} - draw histgram of years of publication 

Usage: #{$0} medline_format_file

Authors: Wubin Qu <quwubin@gmail.com>.
"

if ARGV.size != 1
  $stderr.puts usage
  exit
else
  infile = ARGV[0] # 获取命令行参数，避免把文件名写入代码
  # Input is abstract file in Medline format.
end


year_count = Hash.new(0)
File.read(infile).split("\n\n").each do |article_block|
  article = Bio::MEDLINE.new(article_block)
  
  p article.methods
  exit
#=begin
  if article.affiliations.join(" ") =~ /China/i
    p article.affiliations
    exit
  end
#=end
  
  year_count[article.year.to_i] += 1
end

year_count.keys.sort.each do |year|
  puts "#{year}, #{year_count[year]}"
end