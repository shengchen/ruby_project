#!ruby
# coding: utf-8

usage = "#{$0} - lncRNA-mRNA  corvalue pavlue filter 

Argv1:the cor.value file
Argv2:the output file
Authors: sheng chen <qingchen36@gmail.com>.
"

if ARGV.size != 2
  $stderr.puts usage
  exit
else
  infile = ARGV[0] # 获取命令行参数，避免把文件名写入代码
  outfile = ARGV[1] # 获取命令行参数，避免把文件名写入代码
  # Input is abstract file in Medline format.
end


def get_gene_hash(gene_100)####用来获取gene对应的所有的lncRNA
  gene_hash = Hash.new()
  infile = "paired_cor_pvalue_"
  for i in 1..19 ###测试后进行修改
    temp = infile+i.to_s+".txt"
    #File.open(temp).each_line do |line|
    File.read(temp).split("\n").each do |line|
      fields = line.strip.split("\t")
      if gene_100.key?(fields[0]) then
        if gene_hash.key?(fields[0]) then
           gene_hash[fields[0]] << fields[1..-1]
        else
           gene_hash[fields[0]] = []
        end
      end
    end
    p temp
  end 
  return gene_hash
end

def get_gene_filter(gene_hash)####用来获取gene对应的所有的lncRNA
    filew=File.new("corexp_q0.05.txt","a")
    gene_hash.each do |key, value|
        #p key
        temp = value.sort_by {|lnc_arr| lnc_arr[1].to_f}
        cutoff1 = temp.size*0.05.to_i
        cutoff2 = temp.size*0.95.to_i
        for i in 0..temp.size-1
          #filew.puts(key+"\t"+temp[i][0]+"\t"+temp[i][1]+"\t"+temp[i][2]+"\t"+temp[i][3]+"\n")
          if(i<= cutoff1 or i>=cutoff2) and temp[i][3].to_f<=0.05 then
             #p temp[i]
             filew.puts(key+"\t"+temp[i][0]+"\t"+temp[i][1]+"\t"+temp[i][2]+"\t"+temp[i][3]+"\n")  
          end
        end
    end
    filew.close
    gene_hash={}
end

genelist_file = "paired_cor_pvalue_19.txt"#读取基因list
genelist = Hash.new()
File.read(genelist_file).split("\n").each do |line|
#File.open(genelist_file).each_line do |line|
  fields = line.strip.split("\t")  
  genelist[fields[0]] = ""
  #p fields[0]
end

gene_100=Hash.new()
#p genelist.keys.size
for i in 6001..genelist.keys.size    
    gene_100[genelist.keys[i-1]]=""
    if i%1000==0 then 
       p i
       gene_all_lncRNA = get_gene_hash(gene_100)
       #p gene_all_lncRNA.keys.size
       get_gene_filter(gene_all_lncRNA)
       gene_all_lncRNA.clear
       gene_100.clear
    end
end 
